import os
import numpy as np
import nltk
from sklearn.datasets import fetch_20newsgroups
import seaborn as sbn
from matplotlib import pyplot
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import names
from nltk.stem import WordNetLemmatizer
from sklearn.cluster import KMeans

nltk.data.path.append(os.getcwd() + '\\nltk_data')
# print(names.words()[0:10])
groups = fetch_20newsgroups(data_home=os.getcwd() + "\\scikit_learn_data")


# print(groups.keys())
# print(groups.target)
# print(np.unique(groups.target))
# print(groups['target_names'])

# displays a histogram of the 500 highest word counts
def letters_only(astr: str):
    for c in astr:
        if not c.isalpha():
            return False
    return True


cv = CountVectorizer(stop_words="english", max_features=500)
cleaned = []
all_names = set(names.words())
lemmatizer = WordNetLemmatizer()

for post in groups.data:
    cleaned.append(' '.join([lemmatizer.lemmatize(word.lower())
                             for word in post.split()
                             if letters_only(word)
                             and word not in all_names]))

transformed = cv.fit_transform(cleaned)
# print(cv.get_feature_names())
km = KMeans(n_clusters=20)
km.fit(transformed)
labels = groups.target
pyplot.scatter(labels, km.labels_)
pyplot.xlabel('Newsgroup')
pyplot.ylabel('Cluster')
pyplot.show()
# sbn.distplot(np.log(transformed.toarray().sum(axis=0)))
# pyplot.xlabel('Log Count')
# pyplot.ylabel('Frequency')
# pyplot.title('Distribution Plot of 500 Word Counts')
# pyplot.show()
